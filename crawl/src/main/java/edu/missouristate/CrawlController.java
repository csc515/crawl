package edu.missouristate;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
public class CrawlController {

	private static final String LOCALHOST_SITE = "http://localhost:8080";

	@GetMapping(value = "/")
	public String getIndex() {
		return "index.html";
	}

	@ResponseBody
	@GetMapping(value = "/crawl")
	public String getCrawler() throws Exception {
//	    String html = new RestTemplate().getForObject(LOCALHOST_SITE, String.class);
		// Quick Test
//	    System.out.println(html);

		// Example 1
		// Get access to the DOM object
//	    Document doc = Jsoup.parse(html);
//	    System.out.println("Parse Successful");

		// Example 2
		// Extract the title from the page
//	    System.out.println(doc.title());

		// Example 3
		// Add a paragraph tag to the html and loop through all p tags
//		Elements paragraphs = doc.getElementsByTag("p");
//		for (Element paragraph : paragraphs) {
//			System.out.println(paragraph.text());
//		}

		// Example 4
		// Use Jsoup to load a URL
		Document document = Jsoup.connect(LOCALHOST_SITE).get();
//		System.out.println(document.title());

		// Example 5
//		Element sampleDiv = document.getElementById("myDiv");
//		System.out.println("Data: " + sampleDiv.text());
//		Elements links = sampleDiv.getElementsByTag("a");
//		System.out.println("Links[0].innerHTML: " + links.get(0).html());

		// Example 6
//		Element sampleDiv = document.getElementById("myDiv");
//		Elements links = sampleDiv.getElementsByTag("a");
//		for (Element link : links) {
//			System.out.println("Href: " + link.attr("href"));
//			System.out.println("Text: " + link.text());
//		}

		// Example 7
		// Get pngs from the DOM
//		Elements pngs = document.select("img[src$=.png]");
//		for (Element png : pngs) {
//			System.out.println("Name: " + png.attr("name"));
//			System.out.println("Source: " + png.attr("src"));
//		}

		// Example 8
		// Get first div w/ class="header"
//		Element headerDiv = document.select("div.header").first();
//		System.out.println("Id: " + headerDiv.id());

		// Example 9
		// Grab the first anchor tag after an h3
//		Elements sampleLinks = document.select("h3 > a");
//		for (Element link : sampleLinks) {
//			System.out.println("Text: " + link.text());
//		}

		// Example 10
		// Extract Text
//		Element link = document.select("a").first();
//		System.out.println("Text: " + link.text());

		// Example 11
		// Extract outer and inner HTML
//		Element link = document.select("a").first();
//		System.out.println("Outer HTML: " + link.outerHtml());
//		System.out.println("Inner HTML: " + link.html());
		
		// Example 12
		// Get value for input where id = "token"
		Element tokenInputElement = document.getElementById("token");
		System.out.println(tokenInputElement.attr("value"));

		return "";
	}
}
